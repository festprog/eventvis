// To parse this JSON data, do
//
//     final eventModel = eventModelFromJson(jsonString);

import 'dart:convert';

class EventModel {
  EventModel({
    required this.type,
    required this.date,
    required this.time,
    required this.eventDescription,
    required this.location,
    required this.status,
    required this.comment,
    required this.zoneCode,
  });

  final int type;
  final String date;
  final String time;
  final String eventDescription;
  final List<double> location;
  final int status;
  final String comment;
  final int zoneCode;

  factory EventModel.fromRawJson(String str) => EventModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory EventModel.fromJson(Map<String, dynamic> json) {
    print("\nEVENTO");
    for (var key in json.keys) {
      print("Key: $key = ${json[key]}");
    }
    print("\n\n");
    try {
      final EventModel result = EventModel(
        type: json["type"],
        date: json["date"],
        time: json["time"],
        eventDescription: json["eventDescription"],
        location: List<double>.from(json["location"].map((x) => x.toDouble())),
        status: json["status"],
        comment: json["comment"],
        zoneCode: json["zoneCode"],
      );
      return result;
    } on Exception catch (e) {
      final EventModel result = EventModel(
        type: json["type"],
        date: json["date"],
        time: json["time"],
        eventDescription: json["eventDescription"],
        location: List<double>.from(json["location"].map((x) => x.toDouble())),
        status: json["status"],
        comment: json["comment"],
        zoneCode: json["zoneCode"],
      );
      return result;
    }
  }

  Map<String, dynamic> toJson() => {
        "type": type,
        "date": date,
        "time": time,
        "eventDescription": eventDescription,
        "location": List<dynamic>.from(location.map((x) => x)),
        "status": status,
        "comment": comment,
        "zoneCode": zoneCode,
      };
}
