import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/widgets/events_widget.dart';
import 'package:flutter/material.dart';

class EventsPage extends StatelessWidget {
  const EventsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: EventsWidget(),
      backgroundColor: bgColor,
    );
  }
}
