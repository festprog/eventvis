import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/widgets/change_password_widget.dart';
import 'package:flutter/material.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Padding(
        padding: EdgeInsets.all(0),
        child: Center(child: ChangePasswordWidget()),
      ),
      backgroundColor: bgColor,
    );
  }
}
