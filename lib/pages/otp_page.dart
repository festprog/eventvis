import 'package:eventvis/widgets/otp_widget.dart';
import 'package:flutter/material.dart';

class OtpPage extends StatelessWidget {
  const OtpPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: Padding(
      padding: EdgeInsets.all(5.0),
      child: SingleChildScrollView(child: Center(child: OtpWidget())),
    ));
  }
}
