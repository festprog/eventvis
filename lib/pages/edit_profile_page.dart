import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/widgets/edit_profile_widget.dart';
import 'package:flutter/material.dart';

class EditProfilePage extends StatelessWidget {
  const EditProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Padding(
        padding: EdgeInsets.all(0),
        child: Center(child: EditProfileWidget()),
      ),
      backgroundColor: bgColor,
    );
  }
}
