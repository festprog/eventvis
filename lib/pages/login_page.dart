import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/widgets/login_widget.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Padding(
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(child: Center(child: LoginWidget())),
      ),
      backgroundColor: bgColor,
    );
  }
}
