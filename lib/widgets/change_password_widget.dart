import 'package:eventvis/services/user_services.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';

class ChangePasswordWidget extends StatefulWidget {
  const ChangePasswordWidget({Key? key}) : super(key: key);

  @override
  _ChangePasswordWidgetState createState() => _ChangePasswordWidgetState();
}

class _ChangePasswordWidgetState extends State<ChangePasswordWidget> {
  TextEditingController? controllerOldPass;

  late bool obscureOldPass;

  TextEditingController? controllerNewPass;

  late bool obscureNewPass;

  TextEditingController? controllerNewPass2;

  late bool obscureNewPass2;

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    controllerOldPass = TextEditingController();
    obscureOldPass = false;
    controllerNewPass = TextEditingController();
    obscureNewPass = false;
    controllerNewPass2 = TextEditingController();
    obscureNewPass2 = false;
  }

  @override
  Widget build(BuildContext context) {
    final UserPreferences prefs = UserPreferences();
    final UserServices userServices = UserServices();
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: primaryColor,
        automaticallyImplyLeading: false,
        title: const Text('Cambio de contraseña', textAlign: TextAlign.center, style: titleText1),
        //actions: [],
        centerTitle: false,
        elevation: 2,
      ),
      backgroundColor: bgColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(10, 5, 10, 5),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsetsDirectional.fromSTEB(0, 20, 0, 10),
                      child: Image.asset(
                        'assets/padlock.png',
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Form(
                      key: formKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('\nContraseña actual',
                              textAlign: TextAlign.start, style: subtitleText2),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(0, 5, 0, 0),
                            child: TextFormField(
                              controller: controllerOldPass,
                              autofocus: true,
                              obscureText: !obscureOldPass,
                              decoration: InputDecoration(
                                hintText: 'Contraseña actual\n\n',
                                hintStyle: bodyText2,
                                enabledBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: secondaryTextColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                    bottomLeft: Radius.circular(4.0),
                                    bottomRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: secondaryTextColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                    bottomLeft: Radius.circular(4.0),
                                    bottomRight: Radius.circular(4.0),
                                  ),
                                ),
                                errorBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: borderColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                    bottomLeft: Radius.circular(4.0),
                                    bottomRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedErrorBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: borderColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                    bottomLeft: Radius.circular(4.0),
                                    bottomRight: Radius.circular(4.0),
                                  ),
                                ),
                                suffixIcon: InkWell(
                                  onTap: () => setState(
                                    () => obscureOldPass = !obscureOldPass,
                                  ),
                                  focusNode: FocusNode(skipTraversal: true),
                                  child: Icon(
                                    obscureOldPass
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: secondaryTextColor,
                                    size: 22,
                                  ),
                                ),
                              ),
                              style: bodyText1,
                              keyboardType: TextInputType.visiblePassword,
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return 'El campo es requerido';
                                }

                                if (val.length < 4) {
                                  return 'Se requieren al menos 4 caracteres.';
                                }

                                return null;
                              },
                            ),
                          ),
                          const Text('\nContraseña nueva',
                              textAlign: TextAlign.start, style: subtitleText2),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(0, 5, 0, 0),
                            child: TextFormField(
                              controller: controllerNewPass,
                              autofocus: true,
                              obscureText: !obscureNewPass,
                              decoration: InputDecoration(
                                hintText: 'Contraseña nueva',
                                hintStyle: bodyText2,
                                enabledBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: secondaryTextColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: secondaryTextColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                errorBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: borderColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedErrorBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: borderColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                suffixIcon: InkWell(
                                  onTap: () => setState(
                                    () => obscureNewPass = !obscureNewPass,
                                  ),
                                  focusNode: FocusNode(skipTraversal: true),
                                  child: Icon(
                                    obscureNewPass
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: secondaryTextColor,
                                    size: 22,
                                  ),
                                ),
                              ),
                              style: bodyText1,
                              keyboardType: TextInputType.visiblePassword,
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return 'El campo es requerido';
                                }

                                if (val.length < 4) {
                                  return 'Se requieren al menos 4 caracteres.';
                                }

                                return null;
                              },
                            ),
                          ),
                          const Text('\nRepite la contraseña nueva',
                              textAlign: TextAlign.start, style: subtitleText2),
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(0, 5, 0, 0),
                            child: TextFormField(
                              controller: controllerNewPass2,
                              autofocus: true,
                              obscureText: !obscureNewPass2,
                              decoration: InputDecoration(
                                hintText: 'Repetir contraseña nueva',
                                hintStyle: bodyText2,
                                enabledBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: secondaryTextColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: secondaryTextColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                errorBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: borderColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedErrorBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: borderColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                suffixIcon: InkWell(
                                  onTap: () => setState(
                                    () => obscureNewPass2 = !obscureNewPass2,
                                  ),
                                  focusNode: FocusNode(skipTraversal: true),
                                  child: Icon(
                                    obscureNewPass2
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: secondaryTextColor,
                                    size: 22,
                                  ),
                                ),
                              ),
                              style: bodyText1,
                              keyboardType: TextInputType.visiblePassword,
                              validator: (val) {
                                if (val == null || val.isEmpty) {
                                  return 'El campo es requerido';
                                }

                                if (val.length < 4) {
                                  return 'Se requieren al menos 4 caracteres.';
                                }

                                if (controllerNewPass?.text != controllerNewPass2?.text) {
                                  return 'Las contraseñas no coinciden';
                                }

                                return null;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(flex: 2),
                    Padding(
                      padding: const EdgeInsetsDirectional.fromSTEB(0, 10, 0, 30),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              debugPrint('Click en Cancelar');
                              Navigator.pop(context);
                              // TODO: Implementar el cambio de pantalla
                            },
                            style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                            child: const Text(
                              "Cancelar",
                              style: buttonText2,
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              debugPrint('Click en Cambiar');
                              debugPrint(controllerOldPass?.text);
                              debugPrint(prefs.password);
                              if (controllerOldPass?.text == prefs.password) {
                                userServices.editPassCode(context: context);
                                Navigator.pushNamed(context, "otppass",
                                    arguments: [controllerNewPass2?.text]);
                              } else {
                                const snackBar = SnackBar(
                                  content: Text('La contraseña actual no es correcta.'),
                                );
                                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                              }
                            },
                            style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                            child: const Text(
                              "Cambiar",
                              style: buttonText2,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
