import 'package:eventvis/services/access_services.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/widgets/form_text_input.dart';
import 'package:flutter/material.dart';

class SigninWidget extends StatelessWidget {
  const SigninWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController userController = TextEditingController();
    final TextEditingController passController = TextEditingController();
    final TextEditingController cellController = TextEditingController();
    final TextEditingController mailController = TextEditingController();
    final AccessServices accessService = AccessServices();

    var centralWidget = SafeArea(
        child: SingleChildScrollView(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Registrate!",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: primaryColor,
                )),
            // Text Input Usuario
            FormTextInput(label: "Nombre de usuario", textController: userController),
            FormTextInput(label: "Contraseña", textController: passController, obscure: true),
            // Text Input Contraseña
            /*FormTextInput(
                label: "Celular",
                textController: cellController,
                keyboardType: TextInputType.number),*/
            FormTextInput(
              label: "E-mail",
              textController: mailController,
              keyboardType: TextInputType.emailAddress,
            ),
            // Botón Registrar
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () {
                      debugPrint("Click botón de registro");
                      var user = userController.text;
                      var pass = passController.text;
                      //var cell = cellController.text;
                      var mail = mailController.text;

                      debugPrint("user: $user");
                      debugPrint("pass: $pass");
                      //debugPrint("cell: $cell");
                      debugPrint("mail: $mail");

                      if (user.isNotEmpty && pass.isNotEmpty && mail.isNotEmpty) {
                        // debugPrint("Enviando petición al servicio.");
                        accessService.signin(user: user, password: pass, email: mail);
                        Navigator.pushNamed(context, "otp", arguments: [mail, user]);
                      } else {
                        const snackBar = SnackBar(
                          content: Text('Llena todos los campos.'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        debugPrint("No se envió la petición al servicio.");
                      }
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                    child: const Text("Registrar"),
                  ),
                )),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Text("¿Ya tienes cuenta?"),
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        "Ingresa aquí!",
                        style: TextStyle(color: primaryColor),
                      ))
                ])),
          ],
        ),
      ),
    ));
    return centralWidget;
  }
}
