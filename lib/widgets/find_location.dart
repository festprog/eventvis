import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:maps_launcher/maps_launcher.dart';

import '../theme/theme.dart';

class FindLocation extends StatefulWidget {
  const FindLocation({super.key});

  @override
  State<FindLocation> createState() => _FindLocationState();
}

class _FindLocationState extends State<FindLocation> {
  double lat = 0.0;
  double lng = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                ElevatedButton(
                    onPressed: () async {
                      final List<dynamic> result = await getLocation();
                      bool isOk = result[0];
                      if (isOk) {
                        lat = result[1];
                        lng = result[2];
                        setState(() {});
                      }
                    },
                    child: const Text("Get Location")),
                Text("Latitud=$lat  Longitud=$lng"),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                  onPressed: () => MapsLauncher.launchCoordinates(lat, lng, 'Mi ubicación'),
                  child: const Text('Ver en Maps'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<List> getLocation() async {
    Location location = Location();

    bool serviceEnabled;
    PermissionStatus permissionGranted;
    LocationData _locationData;

    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return [false];
      }
    }

    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return [false];
      }
    }

    _locationData = await location.getLocation();
    final double? lat = _locationData.latitude;
    final double? lng = _locationData.longitude;
    return [true, lat, lng];
  }
}
