import 'package:eventvis/services/access_services.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';

import 'form_text_input.dart';

class LoginWidget extends StatelessWidget {
  const LoginWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final prefs = UserPreferences();
    final TextEditingController userController = TextEditingController();
    final TextEditingController passController = TextEditingController();
    final AccessServices accessService = AccessServices();

    var centralWidget = SafeArea(
        child: SingleChildScrollView(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Visualizador de Eventos.",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: primaryColor,
                )),
            const SizedBox(width: 100, height: 15),
            const Image(image: AssetImage("assets/megaphone.png"), width: 150, height: 150),
            const SizedBox(width: 100, height: 15),
            // Text Input Usuario
            FormTextInput(label: "Nombre de usuario", textController: userController),
            FormTextInput(label: "Contraseña", textController: passController, obscure: true),
            // Botón Ingresar
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () async {
                      debugPrint("Click botón de ingreso.");
                      var user = userController.text;
                      var pass = passController.text;
                      debugPrint("user: $user");
                      debugPrint("pass: $pass");

                      if (user.isNotEmpty && pass.isNotEmpty) {
                        debugPrint("Enviando petición al servicio.");
                        final List<dynamic> result =
                            await accessService.login(user: user, password: pass, context: context);
                        bool isLoged = result[0];
                        if (isLoged) {
                          debugPrint("Cambiando a página de eventos");
                          Navigator.pushNamed(context, "main");
                        }
                      } else {
                        debugPrint("No se envió la petición al servicio.");
                        const snackBar = SnackBar(
                          content: Text('Debe llenar todos los campos'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                    child: const Text("Ingresar"),
                  ),
                )),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Text("¿No tienes cuenta?"),
                  TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "signin");
                      },
                      child: const Text(
                        "Registrate!",
                        style: TextStyle(color: primaryColor),
                      ))
                ])),
          ],
        ),
      ),
    ));
    return centralWidget;
  }
}
