import 'package:flutter/material.dart';

class FormTextInput extends StatelessWidget {
  const FormTextInput(
      {super.key,
      required this.label,
      required this.textController,
      this.keyboardType = TextInputType.name,
      this.obscure = false});

  final String label;
  final bool obscure;
  final TextInputType keyboardType;
  final TextEditingController textController;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: TextField(
          keyboardType: keyboardType,
          controller: textController,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            labelText: label,
          ),
          obscureText: obscure,
        ));
  }
}
