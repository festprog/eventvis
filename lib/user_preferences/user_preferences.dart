import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instancia = UserPreferences._internal();

  factory UserPreferences() {
    return _instancia;
  }

  UserPreferences._internal();

  late SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  String get token {
    return _prefs.getString('token') ?? "";
  }

  set username(String value) {
    _prefs.setString('username', value);
  }

  String get username {
    return _prefs.getString('username') ?? "";
  }

  set email(String value) {
    _prefs.setString('email', value);
  }

  String get email {
    return _prefs.getString('email') ?? "";
  }

  set name(String value) {
    _prefs.setString('name', value);
  }

  String get name {
    return _prefs.getString('name') ?? "";
  }

  set lastname(String value) {
    _prefs.setString('lastname', value);
  }

  String get lastname {
    return _prefs.getString('lastname') ?? "";
  }

  set password(String value) {
    _prefs.setString('password', value);
  }

  String get password {
    return _prefs.getString('password') ?? "";
  }
/*
  set contacts(String value) {
    _prefs.setString('contacts', value);
  }

  String get contacts {
    return _prefs.getString('contacts') ?? "";
  }
   */
}
