import 'dart:convert';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ZoneServices {
  final ip = "http://sistemic.udea.edu.co:4000";
  final prefs = UserPreferences();

  Future<String> listZones() async {
    var headers = {'Authorization': 'Bearer ${prefs.token}', 'Cookie': 'color=rojo'};
    var request = http.Request('GET', Uri.parse('$ip/reto/zonas/zonas/listar'));

    request.headers.addAll(headers);

    http.StreamedResponse response =
        await request.send().timeout(const Duration(milliseconds: 10000));

    debugPrint("Respuesta = ${response.statusCode}");
    if (response.statusCode == 200) {
      final decodedData = json.decode(await response.stream.bytesToString());
      return decodedData.toString();
    } else {
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Error >> ${decodedData['message']}");
      return "Nada para mostrar.";
    }
  }
}
