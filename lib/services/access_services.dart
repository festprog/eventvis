import 'dart:convert';

import 'package:eventvis/services/user_services.dart';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AccessServices {
  final ip = "http://sistemic.udea.edu.co:4000";
  final prefs = UserPreferences();

  Future<List> login({required String user, required String password, required context}) async {
    List result = [false];
    final UserServices userServices = UserServices();
    var headers = {
      'Authorization': 'Basic Zmx1dHRlci1yZXRvOnVkZWE=',
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var request = http.Request('POST', Uri.parse('$ip/reto/autenticacion/oauth/token'));
    request.bodyFields = {'username': user, 'password': password, 'grant_type': 'password'};
    request.headers.addAll(headers);
    try {
      http.StreamedResponse response = await request.send().timeout(const Duration(seconds: 10));

      if (response.statusCode == 200) {
        final Map<String, dynamic> decodeData = json.decode(await response.stream.bytesToString());
        prefs.username = user;
        prefs.token = decodeData["access_token"];
        prefs.password = password;
        debugPrint(prefs.token);
        debugPrint("Inicio de sesión exitoso!");
        userServices.getUserProfile();
        result[0] = true;
      } else {
        final decodedData = json.decode(await response.stream.bytesToString());
        debugPrint("Fallo código ${response.statusCode}");
        debugPrint(response.reasonPhrase);
        final snackBar = SnackBar(
          content: Text('${decodedData["message"]}'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        result[0] = false;
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
      result[0] = false;
    }
    return result;
  }

  Future signin({required String user, required String password, required String email}) async {
    var headers = {'Content-Type': 'application/json'};
    var request = http.Request('POST', Uri.parse('$ip/reto/usuarios/registro/enviar'));
    request.body = json.encode({
      "username": user,
      "password": password,
      "email": email,
    });
    request.headers.addAll(headers);

    try {
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        debugPrint(await response.stream.bytesToString());
      } else {
        debugPrint(response.reasonPhrase);
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<List> otp({
    required context,
    required String username,
    required String code,
  }) async {
    var result = [];
    var request =
        http.MultipartRequest('POST', Uri.parse('$ip/reto/usuarios/registro/confirmar/$username'));
    request.fields.addAll({'codigo': code});

    http.StreamedResponse response = await request.send();

    debugPrint("Respuesta = ${response.statusCode}");
    if (response.statusCode == 200) {
      debugPrint(await response.stream.bytesToString());
      const snackBar = SnackBar(
        content: Text('Usuario registrado con éxito.'),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      result[0] = true;
      return result;
    } else {
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Error >> ${decodedData['message']}");

      final snackBar = SnackBar(
        content: Text("${decodedData['message']}"),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      result[0] = false;
      return result;
    }
  }
}
