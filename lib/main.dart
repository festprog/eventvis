import 'package:eventvis/pages/change_password_page.dart';
import 'package:eventvis/pages/detail_page.dart';
import 'package:eventvis/pages/edit_profile_page.dart';
import 'package:eventvis/pages/events_page.dart';
import 'package:eventvis/pages/login_page.dart';
import 'package:eventvis/pages/main_page.dart';
import 'package:eventvis/pages/otp_page.dart';
import 'package:eventvis/pages/otp_pass_page.dart';
import 'package:eventvis/pages/signin_page.dart';
import 'package:eventvis/pages/zones_page.dart';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:eventvis/pages/fake_home_page.dart';
import 'package:eventvis/widgets/find_location.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = UserPreferences();
  await prefs.initPrefs();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Visualizador de Eventos.',
      debugShowCheckedModeBanner: false,
      routes: {
        "main": (context) => const MainPageWidget(),
        "login": (context) => const LoginPage(),
        "signin": (context) => const SigninPage(),
        "otp": (context) => const OtpPage(),
        "events": (context) => const EventsPage(),
        "detail": (context) => const DetailPage(),
        "editprofile": (context) => const EditProfilePage(),
        "changepass": (context) => const ChangePasswordPage(),
        "otppass": (context) => const OtpPassPage(),
        "fakehome": (context) => const FakeHomePage(),
        "location": (context) => const FindLocation(),
        "zones": (context) => const ZonesPage(),

        // TODO: hacer el editor de contactos
        // TODO: terminar el tema de la vista principal.
      },
      initialRoute: "fakehome",
    );
  }
}
