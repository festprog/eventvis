// ignore_for_file: use_full_hex_values_for_flutter_colors

import 'package:flutter/material.dart';

// Se agrego la mascara para visualización en el IDE
const int mask = 0xFF000000;
const List<Color> palette = [
  Color(0xffbd00 | mask), // 0
  Color(0xff9500 | mask), // 1
  Color(0xff7b00 | mask), // 2
  Color(0xff5f00 | mask), // 3
  Color(0xff0054 | mask), // 4
  Color(0xf72585 | mask), // 5
  Color(0xb5179e | mask), // 6
  Color(0x7209b7 | mask), // 7
  Color(0x560bad | mask), // 8
  Color(0x480ca8 | mask), // 9
  Color(0x3a0ca3 | mask), // 10
  Color(0x3f37c9 | mask), // 11
  Color(0x4361ee | mask), // 12
  Color(0x4895ef | mask), // 13
  Color(0x4cc9f0 | mask), // 14
];

const primaryColor = Color(0x14213d | mask); // 3
const secondaryColor = Color(0xfca311 | mask); // 9
const tertiaryColor = Color(0xf1c47b | mask);
const alternateColor = Color(0xff5963 | mask); // 13
const primaryTextColor = Color(0x101213 | mask);
const secondaryTextColor = Color(0x57636c | mask);
const lightTextColor = Color(0xF6FAFD | mask);
const borderColor = Color(0x404040 | mask);
const bgColor = Color(0xF6FAFD | mask);

const cardBgColor = Color(0x0F2C4E | mask);
const cardTextColor = Color(0xFFFFFF | mask);

const TextStyle titleText1 = TextStyle(
    fontFamily: 'Roboto', fontSize: 24, fontWeight: FontWeight.w500, color: lightTextColor);
const TextStyle titleText2 = TextStyle(
    fontFamily: 'Roboto', fontSize: 22, fontWeight: FontWeight.w500, color: secondaryTextColor);
const TextStyle titleText3 = TextStyle(
    fontFamily: 'Roboto', fontSize: 20, fontWeight: FontWeight.w500, color: primaryTextColor);

const TextStyle subtitleText1 = TextStyle(
    fontFamily: 'Roboto', fontSize: 18, fontWeight: FontWeight.w500, color: primaryTextColor);
const TextStyle subtitleText2 = TextStyle(
    fontFamily: 'Roboto', fontSize: 16, fontWeight: FontWeight.w500, color: secondaryTextColor);

const TextStyle bodyText1 = TextStyle(
    fontFamily: 'Roboto', fontSize: 14, fontWeight: FontWeight.w500, color: primaryTextColor);
const TextStyle bodyText2 = TextStyle(
    fontFamily: 'Roboto', fontSize: 14, fontWeight: FontWeight.w500, color: secondaryTextColor);

const TextStyle buttonText1 = TextStyle(
    fontFamily: 'Roboto', fontSize: 14, fontWeight: FontWeight.w500, color: primaryTextColor);
const TextStyle buttonText2 = TextStyle(
    fontFamily: 'Roboto', fontSize: 16, fontWeight: FontWeight.w500, color: lightTextColor);

const TextStyle formItemStyle = TextStyle(
    fontFamily: 'Roboto', fontSize: 20, fontWeight: FontWeight.bold, color: primaryTextColor);
const TextStyle fieldItemStyle = TextStyle(
    fontFamily: 'Roboto', fontSize: 16, fontWeight: FontWeight.normal, color: secondaryTextColor);

const TextStyle cardText = TextStyle(
    fontFamily: 'Roboto', fontSize: 16, fontWeight: FontWeight.w700, color: cardTextColor);

const TextStyle cardText2 = TextStyle(
    fontFamily: 'Roboto', fontSize: 20, fontWeight: FontWeight.w700, color: cardTextColor);

const TextStyle eventText =
    TextStyle(fontFamily: 'Roboto', fontSize: 16, fontWeight: FontWeight.w700, color: primaryColor);
